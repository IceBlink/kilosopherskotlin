package ru.spbstu.kilosophers

import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque

class NapkinBox(amount: Int) {
    val napkinBox: ConcurrentLinkedDeque<Napkin>

    init {
        napkinBox = ConcurrentLinkedDeque()
        for (i in 0 until amount)
            napkinBox.push(Napkin())
    }

    suspend fun tryTake(who: AbstractKilosopher): Boolean = if (napkinBox.isEmpty()) false else napkinBox.pop().tryTake(who)

    suspend fun tryDrop(who: AbstractKilosopher, napkin: Napkin): Boolean {
        napkinBox.push(napkin)
        return napkin.tryDrop(who)
    }

}