package ru.spbstu.kilosophers


import ru.spbstu.kilosophers.AbstractFork
import ru.spbstu.kilosophers.AbstractKilosopher
import ru.spbstu.kilosophers.University
import ru.spbstu.kilosophers.sample.MyKilosopher

object MyUniversity : University {
    override fun produce(left: AbstractFork, right: AbstractFork, napkinBox: NapkinBox, vararg args: Any): AbstractKilosopher {
        val kilosopher = MyKilosopher(left, right, napkinBox, args[0] as Int)
        left.right = kilosopher
        right.left = kilosopher
        return kilosopher
    }
}