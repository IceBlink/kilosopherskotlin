package ru.spbstu.kilosophers

import java.util.concurrent.atomic.AtomicReference

class Napkin {

    private val ownerReference = AtomicReference<AbstractKilosopher>()

    val owner: AbstractKilosopher?
        get() = ownerReference.get()

    suspend fun tryTake(who: AbstractKilosopher): Boolean {
        who.napkin = this
        return ownerReference.compareAndSet(null, who)
    }

    suspend fun tryDrop(who: AbstractKilosopher): Boolean = ownerReference.compareAndSet(who, null)
}