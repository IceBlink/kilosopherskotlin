package ru.spbstu.kilosophers.sample

import kotlinx.coroutines.sync.Mutex
import ru.spbstu.kilosophers.*
import ru.spbstu.kilosophers.ActionKind.*
import ru.spbstu.kilosophers.sample.SampleKilosopher.State.*
import java.util.concurrent.atomic.AtomicInteger

class MyKilosopher(left: Fork, right: Fork, napkinBox: NapkinBox, val index: Int) : AbstractKilosopher(left, right, napkinBox) {

    internal enum class State {
        WAITS_BOTH,
        WAITS_RIGHT,
        WAITS_NAPKIN,
        EATS,
        HOLDS_NAPKIN,
        HOLDS_BOTH,
        HOLDS_RIGHT,
        THINKS
    }

    private var state = WAITS_BOTH
    private var droppedLeftWhenTriedToAcquireRight = false

    override fun nextAction(): Action {
        return when (state) {
            WAITS_BOTH -> {
                if (forksTaken.get() < 4) {
                    forksTaken.getAndIncrement()
                    TAKE_LEFT(10)
                } else
                    THINK(100)
            }
            WAITS_RIGHT -> {
                TAKE_RIGHT(10)
            }
            WAITS_NAPKIN -> {
                TAKE_NAPKIN(10)
            }
            EATS -> EAT(50)
            HOLDS_NAPKIN -> DROP_NAPKIN(10)
            HOLDS_BOTH -> {
                DROP_LEFT(10)
            }
            HOLDS_RIGHT -> {
                DROP_RIGHT(10)
            }
            THINKS -> THINK(100)
        }
    }

    override fun handleResult(action: Action, result: Boolean) {
        state = when (action.kind) {
            TAKE_LEFT -> if (result) {
                WAITS_RIGHT
            } else {
                forksTaken.decrementAndGet()
                WAITS_BOTH
            }
            TAKE_RIGHT -> if (result) {
                forksTaken.getAndIncrement()
                WAITS_NAPKIN
            } else WAITS_RIGHT
            TAKE_NAPKIN -> {
                if (result) {
                    EATS
                } else WAITS_NAPKIN
            }
            EAT -> HOLDS_NAPKIN
            DROP_NAPKIN -> {
                if (result) {
                    HOLDS_BOTH
                } else HOLDS_NAPKIN
            }
            DROP_LEFT -> {
                if (result) {
                    forksTaken.decrementAndGet()
                    HOLDS_RIGHT
                } else
                    HOLDS_BOTH
            }
            DROP_RIGHT -> if (result) {
                forksTaken.decrementAndGet()
                THINKS
            } else HOLDS_RIGHT
            THINK -> WAITS_BOTH
        }
    }

    override fun toString(): String {
        return "Kilosopher #$index"
    }

    companion object Officiant {

        var forksTaken: AtomicInteger = AtomicInteger(0)
    }
}