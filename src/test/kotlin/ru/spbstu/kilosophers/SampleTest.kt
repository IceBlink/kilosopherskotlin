package ru.spbstu.kilosophers

import kotlinx.coroutines.*
import ru.spbstu.kilosophers.atomic.AtomicForkBox
import ru.spbstu.kilosophers.concurrent.ConcurrentForkBox
import ru.spbstu.kilosophers.sample.MyKilosopher
//import ru.spbstu.kilosophers.sample.SampleUniversity
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.Test
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class SampleTest {

    private fun doTest(university: University, forkBox: ForkBox, kilosopherCount: Int, napkinCount: Int, duration: Int) {
        val forks = MutableList(kilosopherCount) { forkBox.produce() }
        val kilosophers = mutableListOf<AbstractKilosopher>()
        val napkinBox = NapkinBox(napkinCount)
        for (index in 0 until kilosopherCount) {
            val leftFork = forks[index]
            val rightFork = forks[(index + 1) % kilosopherCount]
            val kilosopher = university.produce(leftFork, rightFork, napkinBox, index)
            kilosophers.add(kilosopher)
        }

        val jobs = kilosophers.map { it.act(duration) }
        var owners: List<AbstractKilosopher> = emptyList()

        val controllerJob = GlobalScope.launch {
            do {
                delay(maxOf(100, minOf(duration / 50, 1000)).toLong())
                owners = forks.mapNotNull { it.owner }.distinct()
            } while (owners.size < kilosopherCount)
        }

        runBlocking {
            jobs.forEach { it.join() }
            controllerJob.cancelAndJoin()
        }

        assertNotEquals(kilosopherCount, owners.size, "Deadlock detected, fork owners: $owners")

        for (kilosopher in kilosophers) {
            assertTrue(kilosopher.eatDuration > 0, "Eat durations: ${kilosophers.map { it.eatDuration }}")
        }
        println(kilosophers.map { it.eatDuration })
        MyKilosopher.Officiant.forksTaken = AtomicInteger(0)
    }

//    @Test
//    fun testSampleKilosopherWithConcurrentFork() {
//        doTest(SampleUniversity, ConcurrentForkBox, kilosopherCount = 5, duration = 20000)
//    }
//
//    @Test
//    fun testSampleKilosopherWithAtomicFork() {
//        doTest(SampleUniversity, AtomicForkBox, kilosopherCount = 5, duration = 20000)
//    }

    //    @Test
//    fun testMyKilosopherWithAtomicFork() {
//        doTest(MyUniversity, AtomicForkBox, kilosopherCount = 5, duration = 20000)
//    }
    @Test
    fun testMyKilosopherWithConcurrentFork() {
        doTest(MyUniversity, ConcurrentForkBox, kilosopherCount = 5, napkinCount = 1, duration = 20000)
    }
}